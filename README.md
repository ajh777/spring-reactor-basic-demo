Chapter 10 "`Introducing Reactor`" in book "Spring in Action, 5th edition"

Summary
---
* Reactive programming involves creating pipelines through which data flows.
* The Reactive Streams specification defines four types: Publisher, Subscriber, Subscription, and Transformer (which is a combination of Publisher and Subscriber).
* Project Reactor implements Reactive Streams and abstracts stream definitions into two primary types, Flux and Mono, each of which offers several hundred operations.
* Spring 5 leverages Reactor to create reactive controllers, repositories, REST clients, and other reactive framework support.