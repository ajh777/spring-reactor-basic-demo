package com.ajh.demo;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;
import reactor.util.function.Tuple2;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReactiveDemoApplicationTests {
	@Test
	public void contextLoads() {
	}

	@Test
	public void createFlux_just() {
		Flux<String> fruits = Flux.just("Apple", "Orange", "Grape", "Banana", "Strawberry");
		fruits.subscribe(f -> System.out.println("Here's some fruit: " + f));

		StepVerifier.create(fruits)
			.expectNext("Apple")
			.expectNext("Orange")
			.expectNext("Grape")
			.expectNext("Banana")
			.expectNext("Strawberry")
			.verifyComplete();

		System.out.println("TC passed: createFlux_just");
	}
	
	@Test
	public void createFlux_fromArray() {
		Flux<String> fruits = Flux.fromArray(new String[] {"Apple", "Orange", "Grape", "Banana", "Strawberry"});

		StepVerifier.create(fruits)
			.expectNext("Apple")
			.expectNext("Orange")
			.expectNext("Grape")
			.expectNext("Banana")
			.expectNext("Strawberry")
			.verifyComplete();

		System.out.println("TC passed: createFlux_fromArray");
	}
	
	@Test
	public void createFlux_fromIterable() {
		@SuppressWarnings("serial")
		Flux<String> fruits = Flux.fromIterable(new ArrayList<String>() {{
			add("Apple");
			add("Orange");
			add("Grape");
			add("Banana");
			add("Strawberry");
		}});

		StepVerifier.create(fruits)
			.expectNext("Apple")
			.expectNext("Orange")
			.expectNext("Grape")
			.expectNext("Banana")
			.expectNext("Strawberry")
			.verifyComplete();

		System.out.println("TC passed: createFlux_fromIterable");
	}

	@Test
	public void createFlux_fromStream() {
		Flux<String> fruits = Flux.fromStream(Stream.of("Apple", "Orange", "Grape", "Banana", "Strawberry"));

		StepVerifier.create(fruits)
			.expectNext("Apple")
			.expectNext("Orange")
			.expectNext("Grape")
			.expectNext("Banana")
			.expectNext("Strawberry")
			.verifyComplete();

		System.out.println("TC passed: createFlux_fromStream");
	}

	@Test
	public void createFlux_range() {
		Flux<Integer> ints = Flux.range(1, 5);
		StepVerifier.create(ints)
			.expectNext(1)
			.expectNext(2)
			.expectNext(3)
			.expectNext(4)
			.expectNext(5)
			.verifyComplete();

		System.out.println("TC passed: createFlux_range");
	}

	@Test
	public void createFlux_interval() {
		Flux<Long> longs = Flux.interval(Duration.ofSeconds(1)).take(5);

		StepVerifier.create(longs)
			.expectNext(0L)
			.expectNext(1L)
			.expectNext(2L)
			.expectNext(3L)
			.expectNext(4L)
			.verifyComplete();

		System.out.println("TC passed: createFlux_interval");
	}
	
	@Test
	public void combine_01_mergeFluxes() {
		// Both Flux objects are set to emit at regular rates,
		// the value will be interleaved through merged Flux, resulting in
		// a character, followed by a food, followed by a character, and so forth.
		Flux<String> characterFlux = Flux
			.just("Garfield", "Kojak", "Barbossa")
			.delayElements(Duration.ofMillis(500)); // Emit an entry every 500ms

		Flux<String> foodFlux = Flux
			.just("Lasagna", "Lollipops", "Apples")
			.delaySubscription(Duration.ofMillis(250)) // Won't emit any data until 250ms have passed following a subscription.
			.delayElements(Duration.ofMillis(500)); // Emit an entry every 500ms

		Flux<String> mergedFlux = characterFlux.mergeWith(foodFlux);

		StepVerifier.create(mergedFlux)
			.expectNext("Garfield")
			.expectNext("Lasagna")
			.expectNext("Kojak")
			.expectNext("Lollipops")
			.expectNext("Barbossa")
			.expectNext("Apples")
			.verifyComplete();

		System.out.println("TC passed: combine_01_mergeFluxes");
	}

	@Test
	public void combine_02_zipFluxes() {
		Flux<String> characterFlux = Flux
			.just("Garfield", "Kojak", "Barbossa");

		Flux<String> foodFlux = Flux
			.just("Lasagna", "Lollipops", "Apples");

		Flux<Tuple2<String, String>> zippedFlux =
			Flux.zip(characterFlux, foodFlux);

		StepVerifier.create(zippedFlux)
			.expectNextMatches(p ->
				p.getT1().equals("Garfield") &&
				p.getT2().equals("Lasagna"))
			.expectNextMatches(p ->
				p.getT1().equals("Kojak") &&
				p.getT2().equals("Lollipops"))
			.expectNextMatches(p ->
				p.getT1().equals("Barbossa") &&
				p.getT2().equals("Apples"))
			.verifyComplete();

		System.out.println("TC passed: combine_02_zipFluxes");
	}

	@Test
	public void combine_03_zipFluxesToObject() {
		Flux<String> characterFlux = Flux
			.just("Garfield", "Kojak", "Barbossa");

		Flux<String> foodFlux = Flux
			.just("Lasagna", "Lollipops", "Apples");

		Flux<String> zippedFlux =
			Flux.zip(characterFlux, foodFlux, (c, f) -> c + " eats " + f);

		StepVerifier.create(zippedFlux)
			.expectNext("Garfield eats Lasagna")
			.expectNext("Kojak eats Lollipops")
			.expectNext("Barbossa eats Apples")
			.verifyComplete();

		System.out.println("TC passed: combine_03_zipFluxesToObject");
	}

	@Test
	public void combine_04_firstFlux() {
		Flux<String> slowFlux = Flux.just("tortoise", "snail", "sloth")
			.delaySubscription(Duration.ofMillis(100));
		Flux<String> fastFlux = Flux.just("hare", "cheetah", "squirrel");

		Flux<String> firstFlux = Flux.first(slowFlux, fastFlux);

		StepVerifier.create(firstFlux)
			.expectNext("hare")
			.expectNext("cheetah")
			.expectNext("squirrel")
			.verifyComplete();

		System.out.println("TC passed: combine_04_firstFlux");
	}

	/*
	 * Transforming and filtering reactive streams
	 * */
	@Test
	public void filter_01_skipAFew() {
		Flux<String> skipFlux = Flux.just("one", "two", "skip a few", "ninety nine", "one hundred").skip(3);

		StepVerifier.create(skipFlux)
			.expectNext("ninety nine", "one hundred")
			.verifyComplete();

		System.out.println("TC passed: filter_01_skipAFew");
	}

	@Test
	public void filter_02_skipAFewSeconds() {
		Flux<String> skipFlux = Flux.just("one", "two", "skip a few", "ninety nine", "one hundred")
				.delayElements(Duration.ofSeconds(1))
				.skip(Duration.ofSeconds(4));

		StepVerifier.create(skipFlux)
			.expectNext("ninety nine", "one hundred")
			.verifyComplete();

		System.out.println("TC passed: filter_02_skipAFewSeconds");
	}

	@Test
	public void filter_03_take() {
		Flux<String> nationalParkFlux = Flux.just( "Yellowstone", "Yosemite", "Grand Canyon", "Zion", "Grand Teton")
			.take(3); // Take first 3 items

		StepVerifier.create(nationalParkFlux)
			.expectNext("Yellowstone", "Yosemite", "Grand Canyon")
			.verifyComplete();

		System.out.println("TC passed: filter_03_take");
	}

	@Test
	public void filter_04_takeWithDuration() {
		Flux<String> nationalParkFlux = Flux.just( "Yellowstone", "Yosemite", "Grand Canyon", "Zion", "Grand Teton")
			.delayElements(Duration.ofSeconds(1))
			.take(Duration.ofMillis(3500)); // Take as many items as it can in first 3.5 seconds

		StepVerifier.create(nationalParkFlux)
			.expectNext("Yellowstone", "Yosemite", "Grand Canyon")
			.verifyComplete();

		System.out.println("TC passed: filter_04_takeWithDuration");
	}

	@Test
	public void filter_05_filter() {
		Flux<String> nationalParkFlux = Flux.just("Yellowstone", "Yosemite", "Grand Canyon", "Zion", "Grand Teton")
			.filter(np -> !np.contains(" ")); // Accept values that don't have any spaces

		StepVerifier.create(nationalParkFlux)
			.expectNext("Yellowstone", "Yosemite", "Zion")
			.verifyComplete();

		System.out.println("TC passed: filter_05_filter");
	}

	@Test
	public void filter_06_distinct() {
		Flux<String> animalFlux = Flux.just("dog", "cat", "bird", "dog", "bird", "anteater")
			.distinct(); // Remove duplication

		StepVerifier.create(animalFlux)
			.expectNext("dog", "cat", "bird", "anteater")
			.verifyComplete();

		System.out.println("TC passed: filter_06_distinct");
	}

	// Mapping reactive data
	class Player {
		private String firstName;
		private String lastName;

		public Player(String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
			result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Player) {
				Player p = (Player)obj;
				return this.firstName.equals(p.firstName) && this.lastName.equals(p.lastName);
			}
			return super.equals(obj);
		}

		private ReactiveDemoApplicationTests getOuterType() {
			return ReactiveDemoApplicationTests.this;
		}
	}

	@Test
	public void transform_01_map() {
		Flux<Player> playerFlux = Flux
			.just("Michael Jordan", "Scottie Pippen", "Steve Kerr")
			.map(n -> { // Mapping is performed synchronously
				String[] split = n.split("\\s");
				return new Player(split[0], split[1]);
			});

		StepVerifier.create(playerFlux)
			.expectNext(new Player("Michael", "Jordan"))
			.expectNext(new Player("Scottie", "Pippen"))
			.expectNext(new Player("Steve", "Kerr"))
			.verifyComplete();

		System.out.println("TC passed: transform_01_map");
	}

	@Test
	public void transform_02_flatMap() {
		Flux<Player> playerFlux = Flux
			.just("Michael Jordan", "Scottie Pippen", "Steve Kerr")
			.flatMap(
				n -> Mono.just(n)
				.map(p -> {
					String[] split = p.split("\\s");
					return new Player(split[0], split[1]);
				})
				.subscribeOn(Schedulers.parallel())
			);

		List<Player> playerList = Arrays.asList(
			new Player("Michael", "Jordan"),
			new Player("Scottie", "Pippen"),
			new Player("Steve", "Kerr"));

		StepVerifier.create(playerFlux)
			.expectNextMatches(p -> playerList.contains(p))
			.expectNextMatches(p -> playerList.contains(p))
			.expectNextMatches(p -> playerList.contains(p))
			.verifyComplete();

		System.out.println("TC passed: transform_02_flatMap");
	}

	@Test
	public void buffer_01_buffer() {
		Flux<String> fruitFlux = Flux.just("apple", "orange", "banana", "kiwi", "strawberry");
		Flux<List<String>> bufferedFlux = fruitFlux.buffer(3);  // Buffering values from a reactive Flux into non-reactive List
																// seems counter-productive.

		StepVerifier
			.create(bufferedFlux)
			.expectNext(Arrays.asList("apple", "orange", "banana"))
			.expectNext(Arrays.asList("kiwi", "strawberry"))
			.verifyComplete();

		System.out.println("TC passed: buffer_01_buffer");
	}

	@Test
	public void buffer_02_bufferThenFlatMap() {
		Flux.just("apple", "orange", "banana", "kiwi", "strawberry")
			.buffer(3)
			.flatMap(x ->
				Flux.fromIterable(x)
					.map(y -> y.toUpperCase())
					.subscribeOn(Schedulers.parallel())
					.log() // Log all reactive streams events
			)
			.subscribe();
		System.out.println("TC passed: buffer_02_bufferThenFlatMap");
	}

	@Test
	public void buffer_03_collectList() {
		Flux<String> fruitFlux = Flux.just("apple", "orange", "banana", "kiwi", "strawberry");

		Mono<List<String>> fruitListMono = fruitFlux.collectList();

		StepVerifier
			.create(fruitListMono)
			.expectNext(Arrays.asList( "apple", "orange", "banana", "kiwi", "strawberry"))
			.verifyComplete();

		System.out.println("TC passed: buffer_03_collectList");
	}

	@Test
	public void buffer_04_collectMap() {
		Flux<String> animalFlux = Flux.just("aardvark", "elephant", "koala", "eagle", "kangaroo");

		Mono<Map<Character, String>> animalMapMono = animalFlux.collectMap(a -> a.charAt(0));

		StepVerifier
		.create(animalMapMono)
		.expectNextMatches(map -> {
			return
				map.size() == 3 &&
				map.get('a').equals("aardvark") &&
				map.get('e').equals("eagle") &&
				map.get('k').equals("kangaroo");
		})
		.verifyComplete();

		System.out.println("TC passed: buffer_04_collectMap");
	}

	/*
	 * Performing logic operations on reactive types
	 * */
	@Test
	public void logic_01_all() {
		Flux<String> animalFlux = Flux.just("aardvark", "elephant", "koala", "eagle", "kangaroo");

		Mono<Boolean> hasAMono = animalFlux.all(a -> a.contains("a"));

		StepVerifier.create(hasAMono)
			.expectNext(true)
			.verifyComplete();

		Mono<Boolean> hasKMono = animalFlux.all(a -> a.contains("k"));

		StepVerifier.create(hasKMono)
			.expectNext(false)
			.verifyComplete();

		System.out.println("TC passed: logic_01_all");
	}

	@Test
	public void logic_02_any() {
		Flux<String> animalFlux = Flux.just("aardvark", "elephant", "koala", "eagle", "kangaroo");

		Mono<Boolean> hasAMono = animalFlux.any(a -> a.contains("t"));

		StepVerifier.create(hasAMono)
			.expectNext(true)
			.verifyComplete();

		Mono<Boolean> hasZMono = animalFlux.any(a -> a.contains("z"));

		StepVerifier.create(hasZMono)
			.expectNext(false)
			.verifyComplete();

		System.out.println("TC passed: logic_02_any");
	}
}
